import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

let  nickname = localstorage.getItem("nickname") || ""

export default new Vuex.Store({
  state: {
     nickname
  },
  mutations: {
    setNickname(state,payload){
      console.log(state)
      state.nickname=payload
    }
  },
  actions: {

  }
})
