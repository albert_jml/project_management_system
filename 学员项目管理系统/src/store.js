import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    editIndex: "",
    editRow: ""
  },
  mutations: {
    //用途编辑之后的dom更新所使用的index
    setEditIndex(state, payload) {
      if (isNaN(payload)) {
        throw new Error("you must provide a number or numberString but got a NaN!")
      }
      state.editIndex = payload;
    },
    setEditRow(state, payload) {
      state.editRow = payload
    }
  },
  actions: {
  }
})
