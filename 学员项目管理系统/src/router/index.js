import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/views/home/Home.vue'
import student from '@/views/home/student'
import welcome from '@/views/home/welcome'
// import login from '@/views/login'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: "home",
            component: Home,
            redirect:"/welcome",
            children: [
                {
                    path: "student",
                    component: student
                },
                {
                    path:"welcome",
                    component:welcome
                }
            ]
        },
       {
            path: "/login",
            component: () => import(/* webpackChunkName: "login" */ '../views/login')
        },
        {
            path: "/register",
            component: () => import(/* webpackChunkName: "login" */ '../views/register')
        }
    ]
}) 