import axios from "axios"
//添加一个全局的拦截器
axios.interceptors.request.use((config) => {
    //如果是登入请求就不需要拦截和携带token了
    if (config.url !== "/api/users/login") {
        config.headers['authorization'] = localStorage.getItem("token")
    }
    return config
}, error => {
    console.log(error)
})

export let login = (data) => {
    return axios({
        url: "/api/users/login",
        method: 'post',
        data: data,
        headers: {
            "Content-Type": "application/json"
        }
    })
}

export let register = (data) => {
    return axios({
        url: "/api/users/register",
        method: 'post',
        data: data,
    })
}
export let getstulist = (query = {}) => {
    return axios({
        url: "/api/students/getstulist",
        method: "get",
        params: query
    })
}
export let addstu = (data) => {
    return axios({
        url: "/api/students/addstu",
        method: "post",
        data: data

    })
}
export let delstu = (data) => {
    return axios({
        url: "/api/students/delstu",
        method: "get",
        params: { sId: data }
    })
}

// 获取班级列表
export let getClasses = () => {
    return axios.get("/api/students/getClasses")
}
//更新学员信息
export let  editStuInfo = (updated) => {
return axios({
    url:"/api/students/updatestu",
    method:"post",
    data:updated
})
}
export let verify = () => axios.get("/api/verify")