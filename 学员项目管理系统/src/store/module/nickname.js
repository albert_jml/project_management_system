import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
let  nickname = localStorage.getItem("nickname")
export default new Vuex.Store({
  state: {
nickname
  },
  mutations: {
 setNickname(state,payload){
   state.nickname = payload
 }
  },
  actions: {
  }
})
