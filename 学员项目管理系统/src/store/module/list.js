import * as stu from '@/api'
export default {
    namespaced: true,
    state: {
        getStulist: []
    },
    mutations: {
        setStulist(state, payload) {

            state.getStulist = payload
        }
    },
    actions: {
        getstulist({ commit }) {
            stu.getstulist()
                .then(res => {
                    commit("setStulist", res.data.data)
                })
        }
    },
    getters: {

    }

}