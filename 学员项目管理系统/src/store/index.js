import Vue from 'vue'
import Vuex from "vuex"

import actions from "./actions"
import mutations from "./mutations"
import state from "./state"
import getters from "./getters"

import nickname from "./module/nickname"
import list  from "./module/list"
Vue.use(Vuex);

export default new Vuex.Store({
    namespaced:true,//命名空间,防止属性名冲突
    state:state,
    mutations:mutations,
    actions:actions,
    getters:getters,
    modules:{ //模块化的store
        nickname,
        list
    }
})