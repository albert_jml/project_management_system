import Vue from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

import VueParticles from 'vue-particles'  
Vue.use(VueParticles)  

import './styles/reset.css'
import './styles/element_reset.css'
import './styles/index.css'

Vue.config.productionTip = false

//全局守卫
router.beforeEach((to,from,next)=>{
  let token = localStorage.getItem("token");
  if(token){
    next()
  }else{
    if(to.path=="/login" || to.path=="/register"){
      // console.log(to)
      next()
    }else{
      next({path:"/login"})
    }
  }
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
